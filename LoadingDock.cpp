// CS 115 Assignment 1
// Vincent Chang 91932026

// TODO: figure out why there's a delay between crew arrival

#include "LoadingDock.h"

#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <memory>
#include <random>
#include <regex>
#include <sstream>
#include <string>

#include "Constants.h"
#include "Misc.h"
#include "Train.h"
#include "Crew.h"

bool eventQueueCmp(const Event& lhs, const Event& rhs) {
    return lhs.absTimestamp < rhs.absTimestamp;
}

LoadingDock::LoadingDock(double numHoursToSimulate, Statistics& statistics, bool readFromFile)
    : clock{0.0}
    , simLength{numHoursToSimulate}
    , eventQueue{}
    , stats{statistics}
    , dockStatus{DockStatus::IDLE}
    , trainCounter{0}
    , crewCounter{0}
    , readFromFile{readFromFile}
    , trainSchedule{""}
    , crewReplacementSchedule{""}
    , lastTimestamp{0}
    , lastDockStatus{DockStatus::IDLE}
    , trainQueue{}
{
}

LoadingDock::~LoadingDock() {
    trainSchedule.close();
    crewReplacementSchedule.close();
}

bool LoadingDock::isRunning() const {
    return !eventQueue.empty();
}

double LoadingDock::getCurrentTime() const {
    return clock;
}

ArrivalInfo LoadingDock::lineToArrivalInfo(const std::string& trainScheduleLine) const {
    // regex is probably overkill, but I didn't feel like writing a string splitting function
    std::smatch regexMatch;
    std::regex arrivalRegex{"^(\\d+\\.\\d+) (\\d+\\.\\d+) (\\d+\\.\\d+)$", std::regex::ECMAScript};
    if (!std::regex_match(trainScheduleLine, regexMatch, arrivalRegex)) {
        // TODO: catch this exception
        throw EventNotFoundError{"No schedule match found"};
    }

    double timeArrival = std::stod(regexMatch[1].str());
    double unloadLength = std::stod(regexMatch[2].str());
    double crewRemainingTime = std::stod(regexMatch[3].str());

    return ArrivalInfo{timeArrival, unloadLength, crewRemainingTime};
}

ArrivalInfo LoadingDock::generateArrivalInfoFromFile() {
    if (trainSchedule.eof()) {
        throw EndOfInputFile{"Train schedule input reached end of file but program did not halt."};
    }

    std::string nextScheduledTrain;

    // use safeGetLine defined in misc.h to deal with Linux/Windows/Mac line endings
    safeGetline(trainSchedule, nextScheduledTrain);
    if (nextScheduledTrain.empty()) {
        throw EndOfInputFile{};
    }

    return lineToArrivalInfo(nextScheduledTrain);
}

double LoadingDock::getCrewTravelTimeFromFile() {
    std::string nextCrewReplacementTime;

    if (crewReplacementSchedule.eof()) {
        crewReplacementSchedule.seekg(0); // loop back and read from the top
    }

    safeGetline(crewReplacementSchedule, nextCrewReplacementTime);
    if (nextCrewReplacementTime.empty()) {
        crewReplacementSchedule.seekg(0); // assume blank line means EOF; loop back and use times from top again
        safeGetline(crewReplacementSchedule, nextCrewReplacementTime);
    }

    return std::stod(nextCrewReplacementTime);
}

void LoadingDock::initialize() {
    // generate initial arrival event
    clock = 0.0;
    generateNextArrival(true); // train 0 arrives at time 0.00
}

void LoadingDock::initializeFromFile(const std::string& trainTimes, const std::string& crewTravelTimes) {
    // initialize file objects
    trainSchedule.open(trainTimes);
    crewReplacementSchedule.open(crewTravelTimes);

    clock = 0.0;
    generateNextArrival(true); // train 0 arrives at time 0.00
}

void LoadingDock::nextEvent() {
    Event currentEvent{eventQueue.front()};
    eventQueue.pop_front();

    clock = currentEvent.absTimestamp;

    // create "Time x.xx:" and crew indentation strings
    std::stringstream timeStringStream;
    timeStringStream << std::fixed << std::setprecision(2) << "Time " << currentEvent.absTimestamp << ":";
    std::string timeString{timeStringStream.str()};
    std::string crewTab(timeString.length() + 1, ' ');

    switch (currentEvent.eventType) {
        case EventType::ARRIVAL:
            handleArriveEvent(currentEvent); // generates ENTER_DOCK, EXIT_DOCK, HOG_OUT events

            std::cout << timeString << " train " << currentEvent.train->number
                      << " arrival for " << currentEvent.train->unloadLength << "h of unloading," << std::endl;
            std::cout << crewTab << "crew " << currentEvent.train->crew->number << " with "
                      << currentEvent.train->crew->remainingTime << "h before hogout (Q=" << trainQueue.size() << ")"
                      << std::endl;

            // update statistics
            stats.updateMaxNumTrainsQueued(trainQueue.size());
            break;

        case EventType::ENTER_DOCK:
            handleEnterDockEvent(currentEvent);

            std::cout << timeString << " train " << currentEvent.train->number
                      << " entering dock for " << currentEvent.train->unloadLength << "h of unloading," << std::endl;
            std::cout << crewTab << "crew " << currentEvent.train->crew->number << " with "
                      << currentEvent.train->crew->remainingTime << "h before hogout (Q=" << trainQueue.size() << ")"
                      << std::endl;

            // update statistics
            // currentEvent.train->timeEnterDock is not updated after hogouts
            stats.updateTimeInQueue(currentEvent.absTimestamp - currentEvent.train->timeArrival);
            break;

        case EventType::EXIT_DOCK:
            handleExitDockEvent(currentEvent);

            std::cout << timeString << " train " << currentEvent.train->number
                      << " departing (Q=" << trainQueue.size() << ")" << std::endl;

            // check if next train in queue is hogged out
            if (isQueuedTrainHoggedOut()) {
                std::shared_ptr<Train> nextTrain = trainQueue.front();
                std::cout << timeString << " train " << nextTrain->number << " crew "
                          << crewCounter << " hasn't arrived yet, " << std::endl;
                std::cout << crewTab << "cannot enter dock (SERVER HOGGED)" << std::endl;
            }

            // update statistics
            stats.incrNumTrainsServed();
            stats.updateHoggedOutCounts(currentEvent.train->hogOutCount);
            stats.updateTimeInSystem(currentEvent.absTimestamp - currentEvent.train->timeArrival);
            break;

        case EventType::HOG_OUT:
            handleHogOutEvent(currentEvent);

            if (currentEvent.train->status == TrainStatus::UNLOADING) {
                std::cout << timeString << " train " << currentEvent.train->number << " crew "
                          << currentEvent.train->crew->number
                          << " hogged out during service (SERVER HOGGED)" << std::endl;
            } else if (currentEvent.train->status == TrainStatus::QUEUED) {
                std::cout << timeString << " train " << currentEvent.train->number << " crew "
                          << currentEvent.train->crew->number
                          << " hogged out in queue" << std::endl;
            }
            break;

        case EventType::CREW_ARRIVAL:
            handleCrewArrivalEvent(currentEvent);

            // if the current train is unloading (in the dock) OR if the train is at the front of the queue
            if (currentEvent.train->status == TrainStatus::UNLOADING
                || (dockStatus == DockStatus::IDLE && isTrainAtFront(currentEvent.train)))
            {
                std::cout << timeString << " train " << currentEvent.train->number
                          << " replacement crew " << currentEvent.train->crew->number << " arrives (SERVER UNHOGGED)"
                          << std::endl;
            } else if (currentEvent.train->status == TrainStatus::QUEUED) {
                std::cout << timeString << " train " << currentEvent.train->number
                          << " replacement crew " << currentEvent.train->crew->number << " arrives" << std::endl;
            }
            break;
    }

    // update non-event-dependent statistics
    updateDockStatus();
    stats.updateMaxNumTrainsQueued(trainQueue.size());
}

void LoadingDock::endSimulation() const {
    // TODO: ask prof about when simulation ended timestamp should be
    double lastTime = clock > simLength ? clock : simLength;
    std::cout << "Time " << lastTime << ": simulation ended" << std::endl;
}

void LoadingDock::handleArriveEvent(Event& event) {
    if (readFromFile || event.absTimestamp < simLength) {
        generateNextArrival();
    }
    generateEnterDock(event.train);
    generateExitDock(event.train);
    generateHogOut(event.train);

    event.train->status = TrainStatus::QUEUED; // TODO: take this out?
    event.train->crew->number = crewCounter;

    ++crewCounter;
    trainQueue.push(event.train);

    // sort the events just in case
    eventQueue.sort(eventQueueCmp);
}

void LoadingDock::handleEnterDockEvent(Event& event) {
    dockStatus = DockStatus::BUSY;
    event.train->status = TrainStatus::UNLOADING;
    // update crew time remaining
    if (event.train->timeCrewArrived < 0) {
        event.train->crew->remainingTime -= event.absTimestamp - event.train->timeArrival;
    } else {
        event.train->crew->remainingTime -= event.absTimestamp - event.train->timeCrewArrived;
    }
    trainQueue.pop();
}

void LoadingDock::handleExitDockEvent(Event &event) {
    dockStatus = DockStatus::IDLE;

    // remove any remaining HOG_OUT events for this train
    eventQueue.remove_if(
        [&event](const Event& e) {
            return e.train->number == event.train->number && e.eventType == EventType::HOG_OUT;
        }
    );
}

void LoadingDock::handleHogOutEvent(Event& event) {
    if (event.train->status == TrainStatus::UNLOADING) {
        dockStatus = DockStatus::HOGGED_OUT;
    }

    event.train->isHoggedOut = true;

    double crewArrivalWait;
    if (!readFromFile) {
        std::uniform_real_distribution<> crewArrivalWaitGen{Constants::crewReplacementRange.first,
                                                            Constants::crewReplacementRange.second};
        crewArrivalWait = crewArrivalWaitGen(Constants::rand_engine);
    } else {
        crewArrivalWait = getCrewTravelTimeFromFile();
    }

    // messy boolean logic below

    if (event.train->status == TrainStatus::QUEUED && isTrainAtFront(event.train)) { // if hogged-out train is in the queue and at the front

        // find earliest exit; if another train is currently in dock and this will cause a queue hold-up,
        // the timestamp of the exit event will be less than event.absTimestamp + crewArrivalWait
        auto firstExitEventAfterHogout = findFirstEvent(EventType::EXIT_DOCK);

        // if the exit event happens before the crew arrives, delay all enters and exits except for the previous train
        if (firstExitEventAfterHogout->absTimestamp < event.absTimestamp + crewArrivalWait) {
            // find first enter event; this will be the entry of the hogged-out train.
            // we need this timestamp to determine how long to delay subsequent train enter/exits, since there is a gap
            // between the current train's hogout and enter dock events that we need to get rid of.
            auto firstEnterEventAfterHogout = findFirstEvent(EventType::ENTER_DOCK);
            double hogoutEntryDifference = firstEnterEventAfterHogout->absTimestamp - event.absTimestamp;

            for (Event& e : eventQueue) {
                if (e.eventType == EventType::ENTER_DOCK || e.eventType == EventType::EXIT_DOCK) {
                    if (e.train->number >= event.train->number) { // don't delay previous train's exit event
                        e.absTimestamp += crewArrivalWait - hogoutEntryDifference;
                    }
                }
            }
        }
    } else if (event.train->status == TrainStatus::UNLOADING) { // else if train is in dock and hogged out, delay all enters and exits
        for (Event& e : eventQueue) {
            if (e.eventType == EventType::ENTER_DOCK || e.eventType == EventType::EXIT_DOCK) {
                e.absTimestamp += crewArrivalWait;
            }
        }
    }

    // update timeHoggedOut
    event.train->timeHoggedOut = event.absTimestamp;
    ++event.train->hogOutCount;

    // generate CREW_ARRIVAL event
    generateCrewArrival(event.train, event.absTimestamp + crewArrivalWait);

    // since ARRIVAL, HOG_OUT, and CREW_ARRIVAL events are not affected by a HOG_OUT, list may be
    // significantly out of order at this point; inefficient to keep things in order by hand
    eventQueue.sort(eventQueueCmp);
}

void LoadingDock::handleCrewArrivalEvent(Event &event) {
    if (dockStatus == DockStatus::HOGGED_OUT) {
        dockStatus = DockStatus::BUSY;
    }
    event.train->isHoggedOut = false;

    event.train->timeCrewArrived = event.absTimestamp;
    event.train->crew->number = crewCounter;
    ++crewCounter;

    double timeWaitingForCrew = event.train->timeCrewArrived - event.train->timeHoggedOut;

    event.train->crew->remainingTime = Constants::crewWorkLimit - timeWaitingForCrew; // subtract time waiting for crew from their remaining time
    // generate next HOG_OUT event from now with standard crew worktime
    generateHogOut(event.train);
}

void LoadingDock::generateNextArrival(bool firstArrival) {
    std::shared_ptr<Train> newTrain;
    LoadingDock::eventConstItr firstEventAfterArrivalDelay;
    double arrivalTime;

    if (!readFromFile) {
        double arrivalDelay = 0;
        if (!firstArrival) { // let the first arrival come at time 0.0
            std::exponential_distribution<> arrivalGen{1 / Constants::avgTrainArrivalInterval};
            arrivalDelay = arrivalGen(Constants::rand_engine);
        }

        if (clock + arrivalDelay > simLength) return; // do not generate arrivals past the end of the simulation

        std::uniform_real_distribution<> unloadGen{Constants::trainUnloadRange.first,
                                                   Constants::trainUnloadRange.second};
        double unloadLength = unloadGen(Constants::rand_engine);

        std::uniform_real_distribution<> crewRemainingTimeGen{Constants::crewWorkTimeRange.first,
                                                              Constants::crewWorkTimeRange.second};
        double crewRemainingTime = crewRemainingTimeGen(Constants::rand_engine);

        // crewTravelTime is generated upon hogout

        newTrain = std::make_shared<Train>(trainCounter, unloadLength, clock + arrivalDelay, crewRemainingTime);
        arrivalTime = clock + arrivalDelay;

    } else {
        ArrivalInfo info;
        try {
            info = generateArrivalInfoFromFile();
        } catch (const EndOfInputFile& e) {
            return; // handle possible blank line at end of input file
        }

        newTrain = std::make_shared<Train>(trainCounter, info.unloadLength, info.arrivalTime, info.crewRemainingTime);
        arrivalTime = info.arrivalTime;
    }

    // unfortunately, need to linear search to place event in correct order
    firstEventAfterArrivalDelay = findEventAfterTime(arrivalTime);

    if (firstEventAfterArrivalDelay == eventQueue.cend()) {
        eventQueue.emplace_back(arrivalTime, EventType::ARRIVAL, newTrain);
    } else {
        eventQueue.emplace(std::next(firstEventAfterArrivalDelay)
                , arrivalTime, EventType::ARRIVAL, newTrain);
    }

    ++trainCounter;
}

void LoadingDock::generateEnterDock(std::shared_ptr<Train> train) {
    auto lastExitDockEventItr = findLastEvent(EventType::EXIT_DOCK);

    if (lastExitDockEventItr == eventQueue.cend()) { // if no EXIT_DOCK events in the queue (aka dock is idle)
        train->timeEnterDock = clock;

        eventQueue.emplace_front(clock, EventType::ENTER_DOCK, train);
    } else {
        train->timeEnterDock = lastExitDockEventItr->absTimestamp;

        // emplace() inserts *before* the object pointed to at the iterator, so increment it once for new event position
        eventQueue.emplace(std::next(lastExitDockEventItr),
                           lastExitDockEventItr->absTimestamp, EventType::ENTER_DOCK, train);
    }
}

void LoadingDock::generateExitDock(std::shared_ptr<Train> train) {
    double exitDockTime = train->timeEnterDock + train->unloadLength;
    // unfortunately, need to linearly search to put event in correct order
    auto firstEventAfterFinishUnloadingItr = findEventAfterTime(exitDockTime);
    eventQueue.emplace(firstEventAfterFinishUnloadingItr, exitDockTime, EventType::EXIT_DOCK, train);
}

void LoadingDock::generateHogOut(std::shared_ptr<Train> train) {
    double hogOutTime;
    if (train->timeCrewArrived < 0) { // if train has never hogged out
        hogOutTime = train->timeArrival + train->crew->remainingTime;
    } else {
        hogOutTime = train->timeCrewArrived + train->crew->remainingTime;
    }
    // unfortunately, need to linearly search to put event in correct order
    auto firstEventAfterCrewHogOut = findEventAfterTime(hogOutTime);
    eventQueue.emplace(firstEventAfterCrewHogOut, hogOutTime, EventType::HOG_OUT, train);
}

void LoadingDock::generateCrewArrival(std::shared_ptr<Train> train, double crewArrivalTime) {
    train->timeCrewArrived = crewArrivalTime;

    // too much trouble to try and insert CREW_ARRIVAL event in the correct spot; just insert it at the front and
    // let std::sort in handleHogOutEvent() take care of ordering things.
    eventQueue.emplace_front(crewArrivalTime, EventType::CREW_ARRIVAL, train);
}

bool LoadingDock::isQueuedTrainHoggedOut() const {
    if (trainQueue.empty()) {
        return false;
    }
    std::shared_ptr<Train> queuedTrain = trainQueue.front();
    return queuedTrain->isHoggedOut;
}

bool LoadingDock::isTrainAtFront(std::shared_ptr<Train> train) const {
    return train->number == trainQueue.front()->number;
}

void LoadingDock::updateDockStatus() {
    switch (lastDockStatus) {
        case DockStatus::IDLE:
            stats.incrTimeIdle(clock - lastTimestamp);
            break;
        case DockStatus::BUSY:
            stats.incrTimeBusy(clock - lastTimestamp);
            break;
        case DockStatus::HOGGED_OUT:
            stats.incrTimeHoggedOut(clock - lastTimestamp);
            break;
    }

    lastTimestamp = clock;
    lastDockStatus = dockStatus;
}

LoadingDock::eventConstItr LoadingDock::findEventAfterTime(double absTime) const {
    // return an iterator to use as a pointer to position in eventQueue to insert Event
    for (auto eventItr = eventQueue.cbegin(); eventItr != eventQueue.cend(); ++eventItr) {
        if (eventItr->absTimestamp >= absTime) {
            return eventItr;
        }
    }

    return eventQueue.cend();
}

LoadingDock::eventConstItr LoadingDock::findLastEvent(EventType eventType) const {
    // std::list doesn't support random access, and deep copying of iterators is not possible(?) so must keep track
    // of the indices manually to be able to return an iterator to the position of the last event.
    int i = 0;
    int lastEventIndex = -1;
    for (const auto& event : eventQueue) {
        if (event.eventType == eventType) {
            lastEventIndex = i;
        }

        ++i;
    }

	if (lastEventIndex >= 0) {
		auto lastEventItr = eventQueue.cbegin();
		std::advance(lastEventItr, lastEventIndex);
		return lastEventItr;
	} else {
		return eventQueue.cend();
	}
}

LoadingDock::eventConstItr LoadingDock::findFirstEvent(EventType eventType) const {
    for (auto eventItr = eventQueue.cbegin(); eventItr != eventQueue.cend(); ++eventItr) {
        if (eventItr->eventType == eventType) {
            return eventItr;
        }
    }

    return eventQueue.cend();
}