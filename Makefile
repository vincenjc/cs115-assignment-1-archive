# CS 115 Assignment 1
# Vincent Chang 91932026

CC = g++

_OBJS = $(wildcard *.cpp)
OBJS = $(patsubst %.cpp, $(BINDIR)/%.o, $(_OBJS))

CFLAGS = -std=c++14 -Wall -Wextra
BINDIR = bin
CHECKERDIR = checker-and-example-input

# create bin/ and out/ folders
_bindummy := $(shell mkdir -p $(BINDIR))

all: main

run: main
	./train

upload:
	scp -r *.h *.cpp Makefile CS115_Assignment1_writeup output $(CHECKERDIR) vincenjc@odin.ics.uci.edu:~/115/assignment1

submit: main
	../submit -A 1 -f *.h *.cpp Makefile CS115_Assignment1_writeup output train

checkoutput: main
	./train -s $(CHECKERDIR)/schedule.txt $(CHECKERDIR)/traveltimes.txt > testoutput
	cat testoutput
	python3 $(CHECKERDIR)/checker.py testoutput

runfile: main
	./train -s $(CHECKERDIR)/schedule.txt $(CHECKERDIR)/traveltimes.txt

runfilelong: main
	./train -s $(CHECKERDIR)/schedule_long.txt $(CHECKERDIR)/traveltimes_short.txt

valgrind: main
	valgrind --tool=memcheck --leak-check=full ./train

main: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o train

rebuild: clean main

$(BINDIR)/%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -frv $(BINDIR)/*
	rm -fv train
	rm -fv testoutput
