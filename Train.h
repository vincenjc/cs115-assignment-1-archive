// CS 115 Assignment 1
// Vincent Chang 91932026
// CS 115 Assignment 1

#ifndef ASSIGNMENT1_TRAIN_H
#define ASSIGNMENT1_TRAIN_H

#include <memory>

#include "Crew.h"

enum class TrainStatus {
    QUEUED
    , UNLOADING
    , HOGGED_OUT
};

struct Train {
    Train(int number, double unloadLength, double timeArrival, std::shared_ptr<Crew> crew);
    Train(int number, double unloadLength, double timeArrival, double crewRemainingTime, double crewTravelTime = -1);

    int number;
    double unloadLength;
    double timeArrival;
    double timeEnterDock;
    double timeCrewArrived;
    double timeHoggedOut;
    int hogOutCount;
    TrainStatus status;
    bool isHoggedOut;

    std::shared_ptr<Crew> crew;
};


#endif //ASSIGNMENT1_TRAIN_H
