// CS 115 Assignment 1
// Vincent Chang 91932026
// CS 115 Assignment 1

#include "Train.h"

#include <memory>

Train::Train(int number, double unloadLength, double timeArrival, std::shared_ptr<Crew> crew)
    : number{number}
    , unloadLength{unloadLength}
    , timeArrival{timeArrival}
    , timeEnterDock{-1}
    , timeCrewArrived{-1}
    , timeHoggedOut{-1}
    , hogOutCount{0}
    , status{TrainStatus::QUEUED}
    , isHoggedOut{false}
    , crew(std::move(crew))
{
}

Train::Train(int number, double unloadLength, double timeArrival, double crewRemainingTime, double travelTime)
    : number{number}
    , unloadLength{unloadLength}
    , timeArrival{timeArrival}
    , timeEnterDock{-1}
    , timeCrewArrived{-1}
    , timeHoggedOut{-1}
    , hogOutCount{0}
    , status{TrainStatus::QUEUED}
    , isHoggedOut{false}
    , crew(std::make_shared<Crew>(0, crewRemainingTime, travelTime)) // crew number will be initialized upon arrival
{
}
