// CS 115 Assignment 1
// Vincent Chang 91932026

#ifndef ASSIGNMENT1_CREW_H
#define ASSIGNMENT1_CREW_H

struct Crew {
    Crew(int number, double remainingTime, double travelTime);

    int number;
    double remainingTime;
    double travelTime;
};

#endif //ASSIGNMENT1_CREW_H
