// CS 115 Assignment 1
// Vincent Chang 91932026

#ifndef ASSIGNMENT1_STATISTICS_H
#define ASSIGNMENT1_STATISTICS_H

#include <map>
#include <array>
#include <memory>

class Statistics {
public:
    Statistics();
    ~Statistics() = default;

    double getAvgTimeInSystem() const;
    double getMaxTimeInSystem() const;
    double getAvgTimeInQueue() const;
    double getPcntTimeBusy(double totalTime) const;
    double getPcntTimeIdle(double totalTime) const;
    double getPcntTimeHoggedOut(double totalTime) const;
    int getNumTrainsServed() const;
    long getMaxNumTrainsQueued() const;
    double getAvgIdleTimePerTrain() const;

    void incrNumTrainsServed(int num = 1);
    void updateTimeInSystem(double timeInSystem);
    void incrTimeBusy(double time);
    void incrTimeIdle(double time);
    void incrTimeHoggedOut(double time);
    void updateTimeInQueue(double time);
    void updateMaxNumTrainsQueued(int numTrainsInQueue);
    void updateHoggedOutCounts(int numTimesHoggedOut);

    void printStats(double totalTime) const;

private:
    int numTrainsServed;

    double totalTimeInSystem;
    double maxTimeInSystem;

    double timeBusy;
    double timeIdle;
    double timeHoggedOut;

    double totalTimeInQueue;
    long maxNumTrainsQueued;

    std::map<int, int> hoggedOutCounts;
};

#endif // ASSIGNMENT1_STATISTICS_H