// CS 115 Assignment 1
// Vincent Chang 91932026

#ifndef ASSIGNMENT1_ARRIVALINFO_H
#define ASSIGNMENT1_ARRIVALINFO_H

struct ArrivalInfo {
    ArrivalInfo();
    ArrivalInfo(ArrivalInfo&& info) = default;
    ArrivalInfo& operator=(ArrivalInfo&& info) = default;
    ArrivalInfo(double arrivalTime , double unloadLength , double crewRemainingTime);

    double arrivalTime;
    double unloadLength;
    double crewRemainingTime;
};

#endif //ASSIGNMENT1_ARRIVALINFO_H
