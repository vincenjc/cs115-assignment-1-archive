// CS 115 Assignment 1
// Vincent Chang 91932026

#ifndef ASSIGNMENT1_LOADINGDOCK_H
#define ASSIGNMENT1_LOADINGDOCK_H

#include <list>
#include <queue>
#include <fstream>

#include "ArrivalInfo.h"
#include "Event.h"
#include "Statistics.h"
#include "Train.h"

enum class DockStatus {
    IDLE
    , BUSY
    , HOGGED_OUT
};

class LoadingDock {
    using eventConstItr = std::list<Event>::const_iterator;

public:
    LoadingDock() = delete;
    LoadingDock(double numHoursToSimulate, Statistics& statistics, bool readFromFile);
    LoadingDock(LoadingDock& dock) = delete; // uncopyable member vars
    LoadingDock(LoadingDock&& dock) = default;
    ~LoadingDock();

    bool isRunning() const;
    double getCurrentTime() const;

    void initialize();
    void initializeFromFile(const std::string& trainTimes, const std::string& crewTravelTimes); // TODO
    void nextEvent();
    void endSimulation() const;

private:

    double clock;
    double simLength;
    std::list<Event> eventQueue;
    Statistics& stats;
    DockStatus dockStatus;
    int trainCounter;
    int crewCounter;

    bool readFromFile;
    std::ifstream trainSchedule;
    std::ifstream crewReplacementSchedule;

    double lastTimestamp;
    DockStatus lastDockStatus;

    std::queue<std::shared_ptr<Train>> trainQueue;

    ArrivalInfo lineToArrivalInfo(const std::string& trainScheduleLine) const;
    ArrivalInfo generateArrivalInfoFromFile();
    double getCrewTravelTimeFromFile();

    void handleArriveEvent(Event& event);
    void handleEnterDockEvent(Event& event);
    void handleExitDockEvent(Event& event);
    void handleHogOutEvent(Event& event);
    void handleCrewArrivalEvent(Event& event);

    void generateNextArrival(bool firstArrival = false);
    void generateEnterDock(std::shared_ptr<Train> train);
    void generateExitDock(std::shared_ptr<Train> train);
    void generateHogOut(std::shared_ptr<Train> train);
    void generateCrewArrival(std::shared_ptr<Train> train, double crewArrivalTime);

    bool isQueuedTrainHoggedOut() const;
    bool isTrainAtFront(std::shared_ptr<Train> train) const;
    void updateDockStatus();

    eventConstItr findEventAfterTime(double absTime) const;
    eventConstItr findLastEvent(EventType eventType) const;
    eventConstItr findFirstEvent(EventType eventType) const;
};

bool eventQueueCmp(const Event& lhs, const Event& rhs);

#endif // ASSIGNMENT1_LOADINGDOCK_H