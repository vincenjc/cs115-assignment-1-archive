// CS 115 Assignment 1
// Vincent Chang 91932026

#include "ArrivalInfo.h"

ArrivalInfo::ArrivalInfo()
    : arrivalTime{}
    , unloadLength{}
    , crewRemainingTime{}
{
}

ArrivalInfo::ArrivalInfo(double arrivalTime
                        , double unloadLength
                        , double crewRemainingTime)
    : arrivalTime{arrivalTime}
    , unloadLength{unloadLength}
    , crewRemainingTime{crewRemainingTime}
{
}
