// CS 115 Assignment 1
// Vincent Chang 91932026

#ifndef ASSIGNMENT1_EVENT_H
#define ASSIGNMENT1_EVENT_H

#include <string>
#include <memory>

#include "Train.h"

enum class EventType {
      ARRIVAL
    , ENTER_DOCK
    , HOG_OUT
    , CREW_ARRIVAL
    , EXIT_DOCK
};

struct Event {
    Event() = delete;
    Event(double absTime, EventType event, std::shared_ptr<Train> train);
    Event(const Event& other) = default;
    ~Event() = default;
    Event& operator=(const Event& other) = default;

    double absTimestamp;
    EventType eventType;

    std::shared_ptr<Train> train;
};

const char* eventTypeToString(EventType type);

#endif // ASSIGNMENT1_EVENT_H
