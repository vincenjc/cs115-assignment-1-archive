// CS 115 Assignment 1
// Vincent Chang 91932026

#ifndef ASSIGNMENT1_CONSTANTS_H
#define ASSIGNMENT1_CONSTANTS_H

#include <utility>
#include <random>

struct Constants {
    using Range = std::pair<double, double>;

    static const double defaultNumHours;

    // avgTrainArrivalInterval may be changed by user input
    static double avgTrainArrivalInterval;
    static const Range trainUnloadRange;

    static const double crewWorkLimit;
    static const Range crewWorkTimeRange;
    static const Range crewReplacementRange;

    static std::random_device rd;
    static std::default_random_engine rand_engine;
};

#endif // ASSIGNMENT1_CONSTANTS_H