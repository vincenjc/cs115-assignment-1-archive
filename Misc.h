// CS 115 Assignment 1
// Vincent Chang 91932026

#ifndef ASSIGNMENT1_MISC_H
#define ASSIGNMENT1_MISC_H

#include <stdexcept>
#include <string>
#include <istream>

class InvalidEventError : public std::logic_error {
public:
    explicit InvalidEventError(const std::string& msg = "Invalid event object encountered.")
        : std::logic_error::logic_error{msg}
    {
    }
};

class EventNotFoundError : public std::logic_error {
public:
    explicit EventNotFoundError(const std::string& msg = "Event object not found.")
        : std::logic_error::logic_error{msg}
    {
    }
};

class EmptyEventQueueError : public std::logic_error {
public:
    explicit EmptyEventQueueError(const std::string& msg = "Event queue is empty.")
            : std::logic_error::logic_error{msg}
    {
    }
};

class EndOfInputFile : public std::logic_error {
public:
    explicit EndOfInputFile(const std::string& msg = "Train schedule reached EOF.")
            : std::logic_error::logic_error{msg}
    {
    }
};

// obtained from https://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf
std::istream& safeGetline(std::istream& is, std::string& t);

#endif // ASSIGNMENT1_MISC_H