// CS 115 Assignment 1
// Vincent Chang 91932026

#include "Statistics.h"

#include <iostream>

Statistics::Statistics()
    : numTrainsServed{0}
    , totalTimeInSystem{0}
    , maxTimeInSystem{0}
    , timeBusy{0}
    , timeIdle{0}
    , timeHoggedOut{0}
    , totalTimeInQueue{0}
    , maxNumTrainsQueued{0}
    , hoggedOutCounts{}
{
}

double Statistics::getAvgTimeInSystem() const {
    return totalTimeInSystem / numTrainsServed;
}

double Statistics::getMaxTimeInSystem() const {
    return maxTimeInSystem;
}

double Statistics::getAvgTimeInQueue() const {
    return totalTimeInQueue / numTrainsServed;
}

double Statistics::getPcntTimeBusy(double totalTime) const {
    return timeBusy / totalTime;
}

double Statistics::getPcntTimeIdle(double totalTime) const {
    return timeIdle / totalTime;
}

double Statistics::getPcntTimeHoggedOut(double totalTime) const {
    return timeHoggedOut / totalTime;
}

int Statistics::getNumTrainsServed() const {
    return numTrainsServed;
}

long Statistics::getMaxNumTrainsQueued() const {
    return maxNumTrainsQueued;
}

double Statistics::getAvgIdleTimePerTrain() const {
    return timeIdle / numTrainsServed;
}

void Statistics::incrNumTrainsServed(int num) {
    numTrainsServed += num;
}

void Statistics::updateTimeInSystem(double timeInSystem) {
    totalTimeInSystem += timeInSystem;

    if (timeInSystem > maxTimeInSystem) {
        maxTimeInSystem = timeInSystem;
    }
}

void Statistics::incrTimeBusy(double time) {
    timeBusy += time;
}

void Statistics::incrTimeIdle(double time) {
    timeIdle += time;
}

void Statistics::incrTimeHoggedOut(double time) {
    timeHoggedOut += time;
}

void Statistics::updateTimeInQueue(double time) {
    totalTimeInQueue += time;
}

void Statistics::updateMaxNumTrainsQueued(int numTrainsInQueue) {
    if (numTrainsInQueue > maxNumTrainsQueued) {
        maxNumTrainsQueued = numTrainsInQueue;
    }
}

void Statistics::updateHoggedOutCounts(int numTimesHoggedOut) {
    // values are default-initialized the first time they are referenced by a key
    ++hoggedOutCounts[numTimesHoggedOut];
}

void Statistics::printStats(double totalTime) const {
    std::cout << "Statistics" << std::endl;
    std::cout << "----------" << std::endl;

    std::cout << "Total number of trains served: " << getNumTrainsServed() << std::endl;
    std::cout << "Average time-in-system per train: " << getAvgTimeInSystem() << "h" << std::endl;
    std::cout << "Maximum time-in-system per train: " << getMaxTimeInSystem() << "h" << std::endl;

    // getPcntTime returns a double from [0, 1]
    std::cout << "Dock idle percentage: " << getPcntTimeIdle(totalTime) * 100 << "%" << std::endl;
    std::cout << "Dock busy percentage: " << getPcntTimeBusy(totalTime) * 100 << "%" << std::endl;
    std::cout << "Dock hogged-out percentage: " << getPcntTimeHoggedOut(totalTime) * 100 << "%" << std::endl;

    std::cout << "Average time-in-queue over trains: " << getAvgTimeInQueue() << "h" << std::endl;
    std::cout << "Maximum number of trains in queue: " << getMaxNumTrainsQueued() << std::endl;
    std::cout << "Average idle time per train: " << getAvgIdleTimePerTrain() << "h" << std::endl; // NOTE: I was told in an email from the professor that this statistic would not be graded
    std::cout << "Histogram of hogout count per train:" << std::endl;

    for (const auto& kvpair : hoggedOutCounts) {
        std::cout << "[" << kvpair.first << "]: " << kvpair.second << std::endl;
    }
}
