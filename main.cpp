// CS 115 Assignment 1
// Vincent Chang 91932026

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>

#include "Constants.h"
#include "Event.h"
#include "LoadingDock.h"
#include "Statistics.h"

int main(int argc, char** argv) {
    bool readFromFile;
    double simLength;

    if (argc == 3) { // given avg arrival rate & num hours to simulate
        readFromFile = false;
        simLength = std::stod(argv[2]);
        Constants::avgTrainArrivalInterval = std::stod(argv[1]);
    } else if (argc == 4) { // load arrivals from file
        if (argv[1] != std::string{"-s"}) {
            std::cerr << "Invalid arguments: '-s' not specified." << std::endl;
            return 1;
        }

        readFromFile = true;
        simLength = -1; // simLength is ignored
    } else if (argc == 2) {
        std::cerr << "Invalid arguments: must have 2 or 3 arguments." << std::endl;
        return 1;
    } else {
        readFromFile = false;
        simLength = Constants::defaultNumHours;
    }

    std::cout << std::fixed << std::setprecision(2); // this persists throughout the program

    Statistics stats;
    LoadingDock dock{simLength, stats, readFromFile};

    if (argc == 4) {
        std::string trainSchedule{argv[2]};
        std::string crewTravelTimes{argv[3]};
        dock.initializeFromFile(trainSchedule, crewTravelTimes);
    } else {
        dock.initialize();
    }

    while (dock.isRunning()) {
        dock.nextEvent();
    }
    dock.endSimulation();
    std::cout << std::endl;

    stats.printStats(dock.getCurrentTime());

    return 0;
}