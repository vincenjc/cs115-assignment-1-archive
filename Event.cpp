// CS 115 Assignment 1
// Vincent Chang 91932026

#include "Event.h"

#include <iostream>
#include <memory>

Event::Event(double absTime, EventType event, std::shared_ptr<Train> train)
    : absTimestamp{absTime}
    , eventType{event}
    , train{std::move(train)}
{
}

const char* eventTypeToString(EventType type) {
    switch (type) {
    case EventType::ARRIVAL:
        return "ARRIVAL";
    case EventType::EXIT_DOCK:
        return "EXIT_DOCK";
    case EventType::ENTER_DOCK:
        return "ENTER_DOCK";
    case EventType::CREW_ARRIVAL:
        return "CREW_ARRIVAL";
    case EventType::HOG_OUT:
        return "HOG_OUT";
    }
}
