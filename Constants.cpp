// CS 115 Assignment 1
// Vincent Chang 91932026

#include "Constants.h"

#include <random>
#include <utility>

// use automatic int -> double widening because I'm lazy
// CLion is complaining about uncaught exceptions in static scope but that's fine
// program should crash and halt if an exception is thrown when these are initialized

const double Constants::defaultNumHours = 72000;

double Constants::avgTrainArrivalInterval = 10;
const Constants::Range Constants::trainUnloadRange = std::make_pair(3.5, 4.5);

const double Constants::crewWorkLimit = 12;
const Constants::Range Constants::crewWorkTimeRange = std::make_pair(6, 11);
const Constants::Range Constants::crewReplacementRange = std::make_pair(2.5, 3.5);

std::random_device Constants::rd;
std::default_random_engine Constants::rand_engine{rd()};